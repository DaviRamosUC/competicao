package br.ucsal;

import java.util.Scanner;

public class Problema_B {

	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner (System.in);
		
		int N = sc.nextInt();
		
		int num1= 0, num2= 0, num3= 0, num4= 0, num5= 0;
		
		int []c = new int [N];
		
		int A=0, B=0, C =0;
		
		for (int i = 0; i < N; i++) {
			c [i] = sc.nextInt();
			
			if (c[i]==1) {
				num1+=1;
			}else if (c[i]==2) {
				num2+=1;
			}else if (c[i]==3) {
				num3+=1;
			}else if (c[i]==4) {
				num4+=1;
			}else if(c[i]==5) {
				num5+=1;
			}
			
			while ((num1>0) && (num3>0) && (num5>0)) {
				A++;
				num1--;
				num3--;
				num5--;
			}
			
			while ((num1>0) && (num4>0)) {
				B++;
				num1--;
				num4--;
			}
			
			while ((num2>0) && (num4>0)) {
				C++;
				num2--;
				num4--;
			}
		}
		
		System.out.println("A: "+A);
		System.out.println("B: "+B);
		System.out.println("C: "+C);
		
		sc.close();
		
		

	}

}
