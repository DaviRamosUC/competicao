package br.ucsal;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int N = sc.nextInt();

		int[][] tab = new int[N][N];

		int linha = 0, coluna = 0;

		// ler matriz
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {

				tab[i][j] = sc.nextInt();

			}
		}

		int resultado = 0;
		int somaLinha = 0, somaColuna = 0, somaLinhaMaior = 0, somaColunaMaior = 0;

		for (int a = 0; a < N; a++) {

			for (int i = 0; i < N; i++) {
				if (i != linha) {
					System.out.println("Linha " + tab[i][coluna]);
					somaLinha = somaLinha + tab[i][coluna];

					if (somaLinhaMaior < somaLinha) {
						somaLinhaMaior = somaLinha;
					}
				}
			}
			System.out.println("");
			System.out.println("------------");
			for (int j = 0; j < N; j++) {
				if (j != coluna) {
					System.out.print("Linha " + tab[linha][j]);
					somaColuna = somaColuna + tab[linha][j];

					if (somaColunaMaior < somaColuna) {
						somaColunaMaior = somaColuna;
					}
				}

			}
			resultado = somaLinhaMaior + somaColunaMaior;

			linha++;
			coluna++;
		}
		System.out.println("\n ");
		System.out.println(resultado);
		sc.close();

	}

}
